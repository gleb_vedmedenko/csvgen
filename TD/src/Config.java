import java.io.FileWriter; 
import java.io.FileReader;
import java.util.*;
import com.opencsv.*;
import java.io.IOException;

public class Config extends MyFile {

	
	public Config(String path)
	{
		super("\\Config.csv",path);
		
	
		
		
	}
	
	@Override
	public void write(String[] entries)
	{
		
		try (CSVWriter writer = new CSVWriter(new FileWriter(super.getFullPath()))) {
   		 
            writer.writeNext(entries);
            
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   	
	}
	@Override
	public String[] read()
	{
		 try(CSVReader reader = new CSVReader(new FileReader(super.getFullPath())))
    	 {
    		 List<String[]> rows = reader.readAll();
             String[] str = null;
             for (String[] row: rows) {
            	 str = row;
             }
             return str;
    	 }
    	 catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
		 return null;
	}

}
