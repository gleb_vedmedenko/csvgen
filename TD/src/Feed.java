import java.util.ArrayList;
import java.util.List;

/*
 * Stores an RSS feed
 */
public class Feed {

        final String title;
        final String link;
        final String description;
        final String rss;
        final String source;
        final String date;
        final String author;
        final List<FeedMessage> entries = new ArrayList<FeedMessage>();

        public Feed(String title, String link, String description, String author,
                        String rss, String date,String source) {
                this.title = title;
                this.link = link;
                this.description = description;
                this.rss = rss;
                this.date= date;
                this.source= source;
                this.author = author;
                
        }

        public List<FeedMessage> getMessages() {
                return entries;
        }

        public String getTitle() {
        	
                return title + "Rss";
        }

        public String getLink() {
                return link;
        }

        public String getDescription() {
                return description;
        }

        public String getRss() {
                return rss;
        }

        public String getSource() {
                return source;
        }

        public String getDate() {
                return date;
        }

        @Override
        public String toString() {
                return "Feed [rss=" + rss + " description=" + description
                                + " source= " + source+ ", link= " + link + "Date= "
                                + date+ " title= " + title + "]";
        }

}