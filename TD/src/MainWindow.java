import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.regex.Pattern;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
public class MainWindow extends Application{
	static String config_path = System.getProperty("user.dir");
	static String folder_path = config_path;
	static String url_path = "";
	Config config;
	String[] str = null;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}
	Stage window;
	Scene scene;
	
	@Override
	public void start(Stage primaryStage) throws Exception {

		
		window = primaryStage;
		window.setTitle("CSV Generator");
		
		File file = new File(config_path,"\\Config.csv");
		if(file.exists())
		{
			config = new Config(config_path);
			String[] confs = config.read();
			url_path = confs[1];
			
			
		}
		else
		{
			config= new Config(config_path);
		}
		Text text = new Text("Choose the rss");
		TextField url = new TextField(url_path);
		Text text2 = new Text("Choose the path folder");
		TextField path = new TextField(folder_path);
		Button rss_create = new Button("Create CSV");
		
		rss_create.setOnAction(e -> isURL(url,path));
		VBox layout = new VBox(10);
		
		layout.setPadding(new Insets(20,20, 20, 20));
		layout.getChildren().addAll(text,url,text2,path,rss_create);
		
		scene = new Scene(layout,300,250);
		window.setScene(scene);
		window.show();
	}
	
	private void execute(String url,String path)
	{
		
		RSSFeedParser parser = new RSSFeedParser(url);
		Feed feed = parser.readFeed();
	//	System.out.println(feed);
		String[] entries = null;
		//
		String title = feed.getTitle();
		String[] split = title.split("\\W+");
		String split_title = "Rss ";
		for(String s: split)
			split_title += s + " ";
		System.out.println(split_title);
		MyFile w = new Writer(path,split_title);
		
		String[] configs = {w.getPath(),url};
		config.write(configs);
		for(FeedMessage message: feed.getMessages())
		{
			String descr = message.getDescription();
			String article = message.getTitle();
			String[] split_article = article.split("\\W\\&");
			String[] split_descr = descr.split("\\W\\&");
			String final_descr = addStr(split_descr);
			String final_article = addStr(split_article);
			String s = String.format("%s%s%s%s%s%s", final_article,final_descr,message.getDate(),message.getRss(),message.getAuthor(),message.getLink());
			entries = s.split("(###)");
			
			w.write(entries);
		}
		//w.read();
		//config.read();
	}
	private boolean isURL(TextField text_url,TextField text_path)
	{
		
		try{
			
			String str = text_url.getText();
			String path = text_path.getText();
			URL url = new URL(str);
			execute(str,path);
			
			return true;
		}
		catch(MalformedURLException ex)
		{
			System.out.println("Wrong form of url " + ex);
			return false;
		}
	}
	private String addStr(String[] entries)
	{
		String str = " ";
		for (String s: entries)
			str += s + " ";
		return str;
	}

}
